export interface Env {
	DAILY_HUGO: KVNamespace;
}

function time_since(date: number): number {
	const r = Date.now() - date;
	console.log("time_sine", r);
	return r;
}

type HugoPost = {
	title: string,
	urls: string[],
	timestamp: number,
};

interface RedditResponse {
	kind: string,
	data: {
		children: [{
			kind: string,
			data: {
				url: string,
				created_utc: number,
				title: string,
			} & (
				{ 
					is_gallery: true,
					media_metadata: Record<string, { s: { u: string } }>,
				} | {
					is_gallery?: false,
				}
			)
		}]
	}
};

function replace_amp(s: string): string {
	return s.replaceAll("&amp;", "&");
}

async function fetch_newest_hugo_post(): Promise<HugoPost> {
	const options = {
		headers: new Headers({
			'User-Agent': 'daily hugo bot',
		}),
	};
	let data: RedditResponse = await fetch('https://www.reddit.com/user/Amediocrepillowcase/submitted.json', options)
		.then(req => req.json());
	console.log(data);
	const newest_post_data = data.data.children[0].data;
	return {
		title: newest_post_data.title,
		urls: newest_post_data?.is_gallery
			? Object.values(newest_post_data.media_metadata)
				.map(obj => replace_amp(obj.s.u))
			: [newest_post_data.url],
		timestamp: newest_post_data.created_utc,
	};
}

function webhookBody(post: HugoPost): string {
	return JSON.stringify({
		"username": "Daily Hugo Post",
		"type": "rich",
		"name": post.title,
		"embeds": post.urls.map(url => {
			return {
				"title": post.title,
				"image": {"url": url},
			};
		}),
	});
}

export default {
	async fetch(_: Request): Promise<Response> {
		return new Response("hello world");
	},
	async scheduled(
		controller: ScheduledController,
		env: Env,
		ctx: ExecutionContext
	): Promise<void> {
		const last_post_str = await env.DAILY_HUGO.get("LAST_POST");
		const last_post = last_post_str ? parseInt(last_post_str) : null;
		if (last_post) console.log("last_post", new Date(last_post * 1000)); else console.log("no last_post");
		const wait_time = 0;//12 * 60 * 60 * 1000;
		if (last_post == null || (time_since(last_post * 1000) > wait_time)) {
			const post = await fetch_newest_hugo_post();
			console.log("post timestamp:", post.timestamp);
			console.log("post timestamp later than post:", last_post ? (last_post < post.timestamp) : null);
			const is_hugo_post = post.title.toLowerCase().includes("hugo");
			console.log("is hugo post:", is_hugo_post);
			if (is_hugo_post && (last_post == null || last_post < post.timestamp)) {
				await fetch('https://discord.com/api/webhooks/1071880734068969564/s-W4TFkNUl2AJBijNd8fH9wowsRBr3QRnt0Q6EIIrc8HCCgWPjOaybYji5peJFn58i5b', {
					body: webhookBody(post),
					method: 'POST',
					headers: new Headers({
						'Content-Type': 'application/json'
					}),
				});
				await env.DAILY_HUGO.put("LAST_POST", post.timestamp.toString());
			}
		}
	},
};
